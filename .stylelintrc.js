module.exports = {
  processorts: ['stylelint-processor-styled-components'],
  plugins: ['stylelint-order'],
  extends: [
		'stylelint-config-standard',
    './node_modules/prettier-stylelint/config.js',
    'stylelint-config-styled-components',
	],
  ignoreFiles: [
    '**/node_modules/**',
  ],
  rules: {
    'indentation': 2,
    'string-quotes': 'single',
    'order/properties-alphabetical-order': true,
    'declaration-empty-line-before': null,
    'at-rule-empty-line-before': null
  },
};
