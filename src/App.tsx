import React, { FC } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Home from './Home/componnets/Home';
import Shukketsukakunin from './Home/componnets/Main';
import ScheduleAdd from './Schedule/containers/ScheduleAdd';
import ScheduleEdit from './Schedule/components/ScheduleEdit';
import MemberAdd from './Member/componnets/MemberAdd';
import MemberEdit from './Member/componnets/MemberEdit';

const App: FC = () => (
  <AppFC>
    <FlexColumn>
      <Header>
        <h1>
          <Link to="/">出欠確認マン（仮）</Link>
        </h1>
      </Header>
      <Main>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/schedule/ID" exact component={Shukketsukakunin} />
          <Route path="/schedule/add" component={ScheduleAdd} />
          <Route path="/schedule/edit" component={ScheduleEdit} />
          <Route path="/member/add" component={MemberAdd} />
          <Route path="/member/edit" component={MemberEdit} />
          <Redirect to="/" />
        </Switch>
      </Main>
      <Footer>フッター</Footer>
    </FlexColumn>
  </AppFC>
);

// styled-components
const AppFC = styled.div`
  height: 100vh;
  width: 100vw;
`;

const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`;

const Header = styled.header`
  align-items: center;
  background-color: #db002a;
  color: #fafafa;
  display: flex;
  justify-content: center;
`;

const Main = styled.main`
  flex-grow: 1;
`;

const Footer = styled.footer`
  align-items: center;
  background-color: #db002a;
  color: #fafafa;
  display: flex;
  justify-content: center;
`;

export default App;
