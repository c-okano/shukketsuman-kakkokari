// 登録されたイベント一覧画面、トップ画面
import React, { FC } from 'react';
import { Link } from 'react-router-dom';

import '../../index.css';

const Home: FC<{}> = () => {
  const events: string[] = [
    '/schedule/ID',
    '/schedule/add',
    '/schedule/edit',
    '/member/add',
    '/member/edit',
  ];

  return (
    <div>
      <p>ホーム画面</p>
      <p>
        ここにイベント名をリスト表示して、イベント詳細画面に飛べるようにしたい
      </p>
      <p>今は全画面の表示確認用ページになっています</p>
      <ul>
        {events.map((event) => (
          <li key={event}>
            <Link to={event}>{event}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Home;
