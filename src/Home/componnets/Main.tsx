// イベントの詳細と出欠表がのるアプリケーションのメインとなる画面
import React, { FC } from 'react';

import ScheduleDatails from '../../Schedule/components/ScheduleView';
import AnswerResults from '../../Member/componnets/AnswerResults';
import MembersList from '../../Member/componnets/MemberList';
import ReturnButton from '../../shared/ReturnButton';

const Main: FC<{}> = () => (
  <div>
    <ScheduleDatails />
    <AnswerResults />
    <MembersList />
    <ReturnButton />
  </div>
);

export default Main;
