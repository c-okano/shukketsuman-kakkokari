export interface Member {
  id: number;
  name: string;
  attendance: 'OK' | 'Pending' | 'NG';
  comment?: string;
  trainFare?: number;
}
