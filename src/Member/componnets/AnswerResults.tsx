// 回答結果と超人が増えていくコンポーネント
// いちばん大事なこのアプリのアイデンティティ！！！
import React, { FC } from 'react';
import styled from 'styled-components';

// 長さがnの配列 [0, 1, 2, ... , n] を作る関数
const range = (n: number) => (n < 0 ? [] : Array.from(Array(n), (_, i) => i));

// 超人の代わりにとりあえず7色
// 後で超人の名前にして画像を表示したいです
const choujins: string[] = [
  'pink',
  'tomato',
  'orange',
  'gold',
  'limegreen',
  'skyblue',
  'violet',
];

export interface AnswerResultsProps {
  answerCount?: number; // 回答者数
  totalTrainFare?: number; // 交通費合計
  joinMemberCount?: number; // 参加者の人数
  pendingMemberCount?: number; // 未定者の人数
  absentMemberCount?: number; // 不参加者の人数
}

const AnswerResults: FC<AnswerResultsProps> = ({
  answerCount = 50,
  totalTrainFare = 50000,
  joinMemberCount = 25,
  pendingMemberCount = 5,
  absentMemberCount = 20,
}) => (
  <>
    <Wrapper>
      <InnerWrapper>
        <h3>現在の回答結果</h3>
        <MemberCountWrapper>
          <TableWrapper>
            <dl>
              <dt>回答者数</dt>
              <dd>
                {answerCount}
                <span> 人</span>
              </dd>
            </dl>
            <table>
              <tbody>
                <tr>
                  <th>参加</th>
                  <td>
                    {joinMemberCount}
                    <span> 人</span>
                  </td>
                </tr>
                <tr>
                  <th>未定</th>
                  <td>
                    {pendingMemberCount}
                    <span> 人</span>
                  </td>
                </tr>
                <tr>
                  <th>不参加</th>
                  <td>
                    {absentMemberCount}
                    <span> 人</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </TableWrapper>
          <ChoujinWrapper>
            <p>超人</p>
            <p>超人</p>
            <p>超人</p>
            <p>超人</p>
            <p>超人</p>
            <p>超人</p>
          </ChoujinWrapper>
        </MemberCountWrapper>
        <TotalTrainFareWrapper>
          <table>
            <tbody>
              <tr>
                <th>交通費合計</th>
                <td>{totalTrainFare} 円</td>
              </tr>
            </tbody>
          </table>
        </TotalTrainFareWrapper>
      </InnerWrapper>
    </Wrapper>
  </>
);

const Wrapper = styled.div`
  margin: 0 auto;
  margin-top: 3rem;
  max-width: 960px;
  padding: 0 1rem;
`;

const InnerWrapper = styled.div`
  border-top: 1px solid #ccc;
  padding-top: 1rem;

  & h3 {
    border-left: 4px solid #666;
    padding-left: 0.5rem;
  }
`;

const MemberCountWrapper = styled.div`
  display: flex;
  margin-top: 2rem;
  width: 100%;
  @media (max-width: 600px) {
    flex-direction: column;
  }
`;

const TableWrapper = styled.div`
  margin-right: 2rem;

  & dl {
    align-items: center;
    display: flex;
    margin-bottom: 0.5rem;

    & dt {
      background-color: #999;
      border-radius: 4px;
      color: #fafafa;
      float: left;
      font-size: 0.8rem;
      margin-right: 0.5rem;
      padding: 0.2rem 0.5rem;
    }

    & dd {
      & span {
        font-size: 0.8rem;
      }
    }
  }

  & table {
    border: 1px solid #dadada;
    border-collapse: collapse;
    text-align: center;

    & th {
      background: #f3f5f6;
      border: 1px solid #dadada;
      padding: 0.5rem 1rem;
      white-space: nowrap;
    }

    & td {
      border: 1px solid #dadada;
      padding: 0.5rem 1rem;
      text-align: right;
      width: 6.5rem;

      & span {
        font-size: 0.8rem;
      }
    }
  }
`;

const ChoujinWrapper = styled.div`
  border: 1px solid #dadada;
  flex-grow: 1;
  @media (max-width: 600px) {
    margin-top: 1rem;
  }
`;

const TotalTrainFareWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 2rem;

  & table {
    border: 1px solid #dadada;
    border-collapse: collapse;
    text-align: center;

    & th {
      background: #f3f5f6;
      border: 1px solid #dadada;
      padding: 0.5rem 1rem;
      white-space: nowrap;
    }

    & td {
      border: 1px solid #dadada;
      padding: 0.5rem 1rem;
      text-align: right;
      width: 6.5rem;
    }
  }
`;

export default AnswerResults;
