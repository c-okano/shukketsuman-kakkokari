import React, { FC } from 'react';

import ReturnButton from '../../shared/ReturnButton';

const AttendanceInput: FC = () => (
  <div>
    <p>出欠編集画面</p>
    <ReturnButton />
  </div>
);

export default AttendanceInput;
