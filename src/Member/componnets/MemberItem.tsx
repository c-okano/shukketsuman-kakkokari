// 出欠表の一行
import React, { FC } from 'react';
import styled from 'styled-components';

import { Member } from '../Types';
import Button from '../../shared/Button';

export interface MemberItemProps {
  member?: Member;
  editAttendance?: () => void;
}

const MemberItem: FC<MemberItemProps> = ({
  member = {
    id: 1,
    name: 'テリーマン',
    attendance: 'OK',
    comment: '真の勇気ある超人とは自分自身のことは一番最後に考えられる者！',
    trainFare: 1000,
  },
  editAttendance = () => {
    console.log('出欠の編集をするよ');
  },
}) => (
  <tr>
    <Name>
      {/* 一旦ボタンを仮置 */}
      <Button type="button" onClick={editAttendance} buttonstyle="simple">
        {member.name}
      </Button>
    </Name>
    <td>{member.attendance}</td>
    <Comment>{member.comment}</Comment>
    <TrainFare>{member.trainFare} 円</TrainFare>
  </tr>
);

// styled-components
const Name = styled.td`
  font-size: 0.8rem;
  min-width: 5rem;
  padding: 0;
`;

const Comment = styled.td`
  font-size: 0.8rem;
  padding: 0 0.5rem;
  text-align: left;
`;

const TrainFare = styled.td`
  padding: 0 0.5rem;
  text-align: right;
`;

export default MemberItem;
