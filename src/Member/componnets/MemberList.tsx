// 出欠表
import React, { FC } from 'react';
import styled from 'styled-components';

import MemberItem from './MemberItem';
import { Member } from '../Types';
import Button from '../../shared/Button';

export interface MemberListProps {
  members?: Member[];
  addMember?: () => void;
}

const MemberList: FC<MemberListProps> = ({
  members = [
    {
      id: 1,
      name: 'テリーマン',
      attendance: 'OK',
      comment: '真の勇気ある超人とは自分自身のことは一番最後に考えられる者！',
      trainFare: 1000,
    },
    {
      id: 2,
      name: 'ロビンマスク',
      attendance: 'NG',
      comment:
        '超人年齢28歳のこのオレが超人生命をかけて後世に残る名試合をしてやるぜ!!',
      trainFare: 10,
    },
  ],
  addMember = () => console.log('出欠を追加するよ'),
}) => (
  <>
    <Wrapper>
      <InnerWrapper>
        <TableTitle>出欠表</TableTitle>
        <Table>
          <thead>
            <tr>
              <th>名前</th>
              <th>出欠</th>
              <th>コメント</th>
              <th>交通費</th>
            </tr>
          </thead>
          <tbody>
            {members.map((member) => (
              <MemberItem key={member.id} member={member} />
            ))}
          </tbody>
        </Table>
        <ButtonWrapper>
          <Button type="button" onClick={addMember} buttonstyle="primary">
            出欠を登録する
          </Button>
        </ButtonWrapper>
      </InnerWrapper>
    </Wrapper>
  </>
);

// styled-components
const Wrapper = styled.div`
  margin: 0 auto;
  margin-top: 3rem;
  max-width: 960px;
  padding: 0 1rem;
`;

const InnerWrapper = styled.div`
  border-top: 1px solid #ccc;
  padding-top: 1rem;
`;

const TableTitle = styled.h3`
  border-left: 4px solid #666;
  padding-left: 0.5rem;
`;

const Table = styled.table`
  border: 1px solid #dadada;
  border-collapse: collapse;
  margin-top: 1rem;
  text-align: center;
  width: 100%;

  & thead {
    background: #f3f5f6;
  }

  & th {
    border: 1px solid #dadada;
    padding: 0.5rem 1rem;
    white-space: nowrap;
  }

  & td {
    border: 1px solid #dadada;
  }
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 2rem;
`;

export default MemberList;
