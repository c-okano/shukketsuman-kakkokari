export interface EventDetails {
  eventId?: number;
  eventName?: string;
  eventMemo?: string;
  eventDate?: string;
  eventStartTime?: string;
  eventEndTime?: string;
  replyDeadline?: string;
}
