import { AxiosError } from 'axios';

import * as ActionType from './Constants';
import { EventDetails } from '../Types';

interface PostScheduleParams {
  eventDetails: EventDetails;
}

interface PostScheduleResult {
  eventDetails: EventDetails;
}

// ActionCreator
export const addSchedule = {
  start: (params: PostScheduleParams) => ({
    type: ActionType.SCHEDULE_ADD_START as typeof ActionType.SCHEDULE_ADD_START,
    payload: params,
  }),
  succeed: (params: PostScheduleParams, result: PostScheduleResult) => ({
    type: ActionType.SCHEDULE_ADD_SUCCEED as typeof ActionType.SCHEDULE_ADD_SUCCEED,
    payload: {
      params,
      result,
    },
  }),
  fail: (params: PostScheduleParams, error: AxiosError) => ({
    type: ActionType.SCHEDULE_ADD_FAIL as typeof ActionType.SCHEDULE_ADD_FAIL,
    payload: {
      params,
      error,
    },
    error: true,
  }),
};

export type ScheduleAddAction =
  | ReturnType<typeof addSchedule.start>
  | ReturnType<typeof addSchedule.succeed>
  | ReturnType<typeof addSchedule.fail>;
