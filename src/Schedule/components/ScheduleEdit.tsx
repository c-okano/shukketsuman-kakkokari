// イベント編集画面
import React, { FC } from 'react';
import styled from 'styled-components';

import { EventDetails } from '../Types';
import Button from '../../shared/Button';
import ReturnButton from '../../shared/ReturnButton';

export interface ScheduleEditProps {
  eventDetails?: EventDetails;
  handleChange?: () => void;
  handleSubmit?: () => void;
}

const ScheduleEdit: FC<ScheduleEditProps> = ({
  eventDetails = {
    eventName: 'キン肉マン',
    eventMemo:
      'キン肉マンは1979年に様々な形で連載を続ける人気マンガシリーズです。連載開始当初ははウルトラマンをパロディしたヒーロー系ギャグマンガ路線だったものの、超人同士がその強さなどを競う競技会として開催された第20回超人オリンピック編から格闘路線で展開、徐々にシリアスな展開が増えていき人気を獲得した漫画です。',
    eventDate: '12/34',
    eventStartTime: '5:67',
    eventEndTime: '8:90',
    replyDeadline: '12/34 0:00まで',
  },
  handleChange = () => console.log('ハンドルチェンジ！'),
  handleSubmit = () => console.log('送信ボタンおした！'),
}) => (
  <>
    <Wrapper>
      <HeaderWrapper>
        <h2>イベント編集・削除</h2>
      </HeaderWrapper>
      <Form onSubmit={handleSubmit}>
        <label htmlFor="eventName">
          <p>イベント名</p>
          <input
            type="text"
            name="eventName"
            id="eventName"
            value={eventDetails.eventName}
            onChange={handleChange}
          />
        </label>
        <label htmlFor="eventMemo">
          <p>イベント詳細</p>
          <textarea
            name="eventMemo"
            id="eventMemo"
            rows={5}
            value={eventDetails.eventMemo}
            onChange={handleChange}
            placeholder="イベント詳細"
          />
        </label>
        <TimeWrapper>
          <label htmlFor="eventDate">
            <p>日にち</p>
            <input
              type="text"
              name="eventDate"
              id="eventDate"
              value={eventDetails.eventDate}
              onChange={handleChange}
            />
          </label>
          <label htmlFor="eventStartTime">
            <p>開始時間</p>
            <input
              type="text"
              name="eventStartTime"
              id="eventStartTime"
              value={eventDetails.eventStartTime}
              onChange={handleChange}
            />
          </label>
          <label htmlFor="eventEndTime">
            <p>終了時間</p>
            <input
              type="text"
              name="eventEndTime"
              id="eventEndTime"
              value={eventDetails.eventEndTime}
              onChange={handleChange}
            />
          </label>
        </TimeWrapper>
        <label htmlFor="replyDeadline">
          <p>回答期限</p>
          <input
            type="text"
            name="replyDeadline"
            id="replyDeadline"
            value={eventDetails.replyDeadline}
            onChange={handleChange}
          />
        </label>
        <ButtonWrapper>
          <Button type="submit" buttonstyle="primary">
            編集内容を保存する
          </Button>
        </ButtonWrapper>
      </Form>
      <ReturnButton />
    </Wrapper>
  </>
);

// styled-components
const Wrapper = styled.div`
  margin: 0 auto;
  margin-top: 2rem;
  max-width: 600px;
  padding: 0 1rem;
`;

const HeaderWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;

  & h2 {
    border-bottom: 1px solid #333;
    flex-grow: 1;
  }
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;

  & label {
    display: flex;
    flex-direction: column;
    margin-top: 2em;

    & p {
      font-size: 1.17em;
      font-weight: bold;
    }
  }

  & input,
  & textarea {
    border: 1px solid #ccc;
    border-radius: 4px;
    font-size: 1rem;
    margin-top: 0.5rem;
    padding: 4px;
  }
`;

const TimeWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 600px) {
    flex-direction: column;
  }
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 3rem;
`;

export default ScheduleEdit;
