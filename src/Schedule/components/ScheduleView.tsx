// 登録したイベントの詳細画面
import React, { FC } from 'react';
import styled from 'styled-components';

import Button from '../../shared/Button';
import { EventDetails } from '../Types';

export interface ScheduleViewProps {
  eventDetails?: EventDetails;
  editScheduleDetails?: () => void;
  isLoading?: boolean;
}

const ScheduleView: FC<ScheduleViewProps> = ({
  eventDetails = {
    eventId: 0,
    eventName: '超人イベントタイトル',
    eventMemo: 'この世に完璧なものがひとつだけある…それは正義超人の友情さ！',
    eventDate: '13月32日（曜日）',
    eventStartTime: '12:34',
    eventEndTime: '56:78',
    replyDeadline: '13月32日（曜日） 00:00',
  },
  editScheduleDetails = () => {
    console.log('編集ボタンを押したよ');
  },
  isLoading = false,
}) => (
  <>
    <Wrapper>
      <HeaderWrapper>
        <h2>{eventDetails.eventName}</h2>
        <Button
          type="button"
          onClick={editScheduleDetails}
          buttonstyle="secondary"
        >
          編集
        </Button>
      </HeaderWrapper>
      <EventDatailWrapper>
        <h3>日時</h3>
        <p>
          {eventDetails.eventDate} {eventDetails.eventStartTime} ~{' '}
          {eventDetails.eventEndTime}
        </p>
      </EventDatailWrapper>
      <EventDatailWrapper>
        <h3>詳細</h3>
        <p>{eventDetails.eventMemo}</p>
      </EventDatailWrapper>
      <WarningWrapper>
        <h3>回答期限</h3>
        <p>{eventDetails.replyDeadline}</p>
      </WarningWrapper>
    </Wrapper>
  </>
);

// styled-components
const Wrapper = styled.div`
  margin: 0 auto;
  margin-top: 2rem;
  max-width: 960px;
  padding: 0 1rem;
`;

const HeaderWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;

  & h2 {
    border-bottom: 1px solid #333;
    flex-grow: 1;
    margin-right: 1rem;
  }
`;

const EventDatailWrapper = styled.div`
  margin-top: 2rem;

  & h3 {
    border-left: 4px solid #666;
    padding-left: 0.5rem;
  }

  & p {
    margin-top: 0.5rem;
    padding-left: calc(4px + 0.5rem);
  }
`;
const WarningWrapper = styled.div`
  border-top: 1px solid #ccc;
  margin-top: 3rem;
  padding-top: 1rem;

  & h3 {
    border-left: 4px solid #cb360d;
    padding-left: 0.5rem;
  }

  & p {
    margin-top: 0.5rem;
    padding-left: calc(4px + 0.5rem);
  }
`;

export default ScheduleView;
