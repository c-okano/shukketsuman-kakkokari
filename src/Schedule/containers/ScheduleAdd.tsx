import React, { FC, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import ScheduleAdd, { ScheduleAddProps } from '../components/ScheduleAdd';
import { addSchedule } from '../actions/ScheduleAdd';
import { ScheduleAddState } from '../reducers/ScheduleAdd';
import { EventDetails } from '../Types';

interface StateProps {
  // eventDetails: EventDetails;
  isLoading?: boolean;
  // handleChange: () => void;
  // handleSubmit: () => void;
}

interface DispatchProps {
  scheduleAddStart: (eventDetails: EventDetails) => void;
}

type EnhancedScheduleAddProps = ScheduleAddProps & StateProps & DispatchProps;

const mapStateToProps = (state: ScheduleAddState): StateProps => ({
  // eventDetails: state.eventDetails,
  isLoading: state.isLoading,
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps =>
  bindActionCreators(
    {
      scheduleAddStart: (eventDetails: EventDetails) =>
        addSchedule.start({ eventDetails }),
    },
    dispatch,
  );

const ScheduleAddContainer: FC<EnhancedScheduleAddProps> = ({
  // eventDetails,
  isLoading,
  scheduleAddStart,
}) => {
  // フォームの入力部分
  // 格好悪いので後でなんとかしたい
  const [inputEventName, setInputEventName] = useState<string>('');
  const [inputEventMemo, setInputEventMemo] = useState<string>('');
  const [inputEventDate, setInputEventDate] = useState<string>('');
  const [inputEventStartTime, setInputEventStartTime] = useState<string>('');
  const [inputEventEndTime, setInputEventEndTime] = useState<string>('');
  const [inputReplyDeadline, setInputReplyDeadline] = useState<string>('');

  const inputEventDetails: EventDetails = {
    eventName: inputEventName,
    eventMemo: inputEventMemo,
    eventDate: inputEventDate,
    eventStartTime: inputEventStartTime,
    eventEndTime: inputEventEndTime,
    replyDeadline: inputReplyDeadline,
  };

  const handleInputEventNameChange = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setInputEventName(e.target.value);
  };
  const handleInputEventMemoChange = (
    e: React.ChangeEvent<HTMLTextAreaElement>,
  ) => {
    setInputEventMemo(e.target.value);
  };
  const handleInputEventDateChange = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setInputEventDate(e.target.value);
  };
  const handleInputEventStartTimeChange = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setInputEventStartTime(e.target.value);
  };
  const handleInputEventEndTimeChange = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setInputEventEndTime(e.target.value);
  };
  const handleInputReplyDeadlineChange = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setInputReplyDeadline(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    scheduleAddStart(inputEventDetails);
  };

  return (
    <ScheduleAdd
      inputEventDetails={inputEventDetails}
      handleInputEventNameChange={handleInputEventNameChange}
      handleInputEventMemoChange={handleInputEventMemoChange}
      handleInputEventDateChange={handleInputEventDateChange}
      handleInputEventStartTimeChange={handleInputEventStartTimeChange}
      handleInputEventEndTimeChange={handleInputEventEndTimeChange}
      handleInputReplyDeadlineChange={handleInputReplyDeadlineChange}
      handleSubmit={handleSubmit}
    />
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScheduleAddContainer);
