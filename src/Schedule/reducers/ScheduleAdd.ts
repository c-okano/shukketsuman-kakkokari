import { Reducer } from 'redux';
import { AxiosError } from 'axios';

import { ScheduleAddAction } from '../actions/ScheduleAdd';
import * as ActionType from '../actions/Constants';
import { EventDetails } from '../Types';

export interface ScheduleAddState {
  eventDetails: EventDetails;
  isLoading?: boolean;
  error?: AxiosError | null;
}

// Storeの初期状態
export const initialState = {
  eventDetails: {
    eventId: 0,
    eventName: '超人が使用する技',
    eventMemo:
      'プロレス技を基本とするが、人間を超えた能力を持つ超人により放たれるため、その破壊力はすさまじいものがある。',
    eventDate: '1/23',
    eventStartTime: '4:56',
    eventEndTime: '7:89',
    replyDeadline: 'ストアの初期データ',
  },
  isLoading: false,
};

// Reducer (prevState, action) => newState
const scheduleAddReducer: Reducer<ScheduleAddState, ScheduleAddAction> = (
  state: ScheduleAddState = initialState,
  action: ScheduleAddAction,
) => {
  switch (action.type) {
    case ActionType.SCHEDULE_ADD_START:
      return {
        ...state,
        eventDetails: {},
        isLoading: true,
      };
    case ActionType.SCHEDULE_ADD_SUCCEED:
      return {
        ...state,
        eventDetails: action.payload.result.eventDetails,
        isLoading: false,
      };
    case ActionType.SCHEDULE_ADD_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
      };
    default:
      // eslint-disable-next-line no-case-declarations, @typescript-eslint/no-unused-vars
      const _: never = action;

      return state;
  }
};

export default scheduleAddReducer;
