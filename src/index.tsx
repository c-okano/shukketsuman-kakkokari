import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import 'ress';
import App from './App';
import * as serviceWorker from './serviceWorker';
import scheduleAddReducer, {
  initialState,
} from './Schedule/reducers/ScheduleAdd';

const store = createStore(scheduleAddReducer, initialState);
const checkStore = () => console.log(store.getState());

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
        <button type="button" onClick={checkStore}>
          ストアを確認
        </button>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
