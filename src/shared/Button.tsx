/* eslint-disable @typescript-eslint/no-unused-vars */
// 汎用的なボタン
/* eslint-disable react/button-has-type */
import React, { FC } from 'react';
import styled, { css } from 'styled-components';

// styled-components用
// TypeAliasを使ってButtonStyleという名前をつけておく
type ButtonStyle = 'primary' | 'secondary' | 'simple' | 'danger';

export interface StyleProps {
  buttonstyle?: ButtonStyle;
}

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & StyleProps;

const Button: FC<Props> = (props) => <ButtonComponent {...props} />;

// styled-components
// ボタンのスタイルを動的に変化させる関数
const getStyle = (style: ButtonStyle = 'secondary') => {
  switch (style) {
    case 'primary':
      return css`
        background-color: tan;
        color: white;
        font-size: 1.2rem;
        padding: 1rem 4rem;
      `;
    case 'secondary':
      return css`
        background-color: transparent;
        border: 1px solid #666;
        color: #333;
        font-size: 0.8rem;
        padding: 0.8rem 1rem;
      `;
    case 'simple':
      return css`
        background-color: transparent;
        border: none;
        color: #333;
        font-size: 0.8rem;
        padding: 0.8rem 1rem;
      `;
    case 'danger':
      return css`
        background-color: black;
        color: white;
        font-size: 0.8rem;
        padding: 0.8rem 1rem;
      `;
    default:
      // caseに漏れがないかチェックする
      // eslint-disable-next-line no-case-declarations
      const _: never = style;

      return null;
  }
};

const ButtonComponent = styled.button`
  border-radius: 4px;
  cursor: pointer;
  display: inline-block;
  font-weight: bold;
  line-height: 1;
  text-decoration: none;
  transition: opacity 0.3s;
  ${(props: StyleProps) => getStyle(props.buttonstyle)}

  &:hover {
    opacity: 0.7;
  }
`;

export default Button;
