// ブラウザの戻るボタン
/* eslint-disable react/button-has-type */
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

type ReturnButtonProps = {} & RouteComponentProps;

const ReturnButton: FC<ReturnButtonProps> = ({ history }) => (
  <>
    <ReturnButtonComponent
      onClick={() => {
        history.go(-1);
      }}
    >
      戻る
    </ReturnButtonComponent>
  </>
);

// styled-components
const ReturnButtonComponent = styled.button`
  border: 1px solid #666;
  border-radius: 4px;
  color: #666;
  cursor: pointer;
  display: inline-block;
  font-size: 0.8rem;
  font-weight: bold;
  line-height: 1;
  padding: 0.8rem 1rem;
  text-decoration: none;
`;

export default withRouter(ReturnButton);
